module.exports = {
    env: {
      browser: true,
      es2021: true
    },
    extends: [
      'standard',
      'plugin:svelte/recommended'
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
      project: 'tsconfig.json',
      extraFileExtensions: ['.svelte']
    },
    plugins: [
      '@typescript-eslint'
    ],
    rules: {
    },
    overrides: [
      {
        files: ['*.svelte'],
        parser: 'svelte-eslint-parser',
        parserOptions: {
          parser: {
            ts: '@typescript-eslint/parser',
            typescript: '@typescript-eslint/parser'
          }
        }
      }
    ]
  }
  