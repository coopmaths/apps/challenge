import shuffleAndConcat from './shuffleAndConcat'

export default function createQuestionsTables ({
  numberOfQuestions = 10,
  tables = new Set([2, 3, 4, 5, 6, 7, 8, 9, 10]),
  min = 2,
  max = 10,
  tableIsFirst = true
} = {}) {
  const questions = []
  if (tables.size === 0) tables = new Set([2, 3, 4, 5, 6, 7, 8, 9, 10])
  const tablesArray = Array.from(tables)
  const tablesMixed = shuffleAndConcat(tablesArray, numberOfQuestions)
  const numbersMixed = shuffleAndConcat(Array(max - min + 1).fill(0).map((_, i) => i + min), numberOfQuestions)
  for (let i = 0; i < numberOfQuestions; i++) {
    const table = tablesMixed[i]
    const number = numbersMixed[i]
    const text = tableIsFirst ? `${table} × ${number} = ` : `${number} × ${table} = `
    const answer = table * number
    questions.push({ text, answer })
  }
  return questions
}
