type operation = '+' | '-' | '*' | '/'
interface QuestionParams {max?: number, min?: number, avoid?: number[], hasParenthesis?: boolean, operation?: operation, resultIsPositif?: boolean}
interface SetQuestionsParams {numberOfQuestions?: number, operations?: operation[]}

export default function createQuestions ({ min = -10, max = 10, avoid = [0], hasParenthesis = false, numberOfQuestions = 10, operations = ['+'] }: QuestionParams & SetQuestionsParams): {text: string, answer: number}[] {
  const operationsMixed = Array(Math.ceil(numberOfQuestions / operations.length)).fill(operations).flat().sort(() => Math.random() - 0.5)
  const signsMixed = Array(Math.ceil(numberOfQuestions / 2)).fill([true, false]).flat().sort(() => Math.random() - 0.5)
  const questions: {text: string, answer: number}[] = []
  for (let i = 0; i < numberOfQuestions; i++) {
    const question = createQuestion({ operation: operationsMixed[i], min, max, avoid, hasParenthesis, resultIsPositif: signsMixed[i] })
    questions.push(question)
  }
  return questions
}

function createQuestion ({ operation, min, max, avoid, hasParenthesis, resultIsPositif } : QuestionParams) {
  let a = randInt(min, max, avoid)
  let b = randInt(min, max, avoid)
  if (operation === '/') {
    a = a * b
  }
  let answer = getAnswer({ a, b, operation })
  if (resultIsPositif && answer < 0) {
    if (['+', '-'].includes(operation)) {
      a *= -1
      b *= -1
    } else {
      if (randInt(1, 2) === 1) {
        a *= -1
      } else {
        b *= -1
      }
    }
  }
  if (operation === '+' && a > 0 && b > 0) {
    if (Math.random() > 0.5) {
      a = randInt(2, max)
      b = randInt(-a, -1)
    } else {
      b = randInt(2, max)
      a = randInt(-b, -1)
    }
    if (!resultIsPositif) {
      a *= -1
      b *= -1
    }
  }
  answer = getAnswer({ a, b, operation })
  let text = ''
  if (hasParenthesis) {
    text = `${writeParenthesisAndSign(a)} ${operation} ${writeParenthesisAndSign(b)} = `
  } else if (operation === '-') {
    text = `${a} - ${writeParenthesisAndSign(b)} = `
  } else {
    text = `${a} ${operation} ${writeParenthesisIfNegatif(b)} = `
  }
  text = text.replace('*', '×')
  text = text.replace('/', '÷')
  return { text, answer }
}

function randInt (min: number, max: number, avoid: number[] = []) {
  let result = Math.floor(Math.random() * (max - min + 1) + min)
  while (avoid.includes(result)) {
    result = Math.floor(Math.random() * (max - min + 1) + min)
  }
  return result
}

function getAnswer ({ a, b, operation } : {a: number, b: number, operation: operation}) {
  switch (operation) {
    case '+':
      return a + b
    case '-':
      return a - b
    case '*':
      return a * b
    case '/':
      return Math.round(a / b)
  }
}

function writeParenthesisIfNegatif (a: number) {
  if (a < 0) {
    return `(${a})`
  } else {
    return a
  }
}

function writeParenthesisAndSign (a: number) {
  if (a < 0) {
    return `(${a})`
  } else {
    return `(+${a})`
  }
}
